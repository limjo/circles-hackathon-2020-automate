from flask import Flask, request
from api_flow import make_flow
import api_flow_config

port = api_flow_config.api_flows["port"]

app = Flask(__name__)


@app.route('/')
def health():
    return "healthy"


@app.route('/api-flow',  methods=['POST', 'DELETE'])
def api_flow():
    if request.method == 'POST':
        print("received make flow request")
        data = request.get_json()
        print(data)
        return make_flow(data["flow_name"], data["api_task_params"])


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=port)
