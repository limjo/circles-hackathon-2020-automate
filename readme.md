https://docs.prefect.io/core/getting_started/installation.html

```
source bin/activate # set up pyenv and virtual env first
pip install -r requirements.txt
prefect backend server
prefect server start
prefect create project "AutoMate"
prefect agent start
```

Register flow
```
python register_flows.py
```

Start callback server
```
python callback_server.py
```

## Manual testing

trigger api

```
python trigger_job.py
```

# api flows

## model

`api_flow.py`

## create flow from script

`python api_flow_example.py`

## Run api flow server

`python api_flows_server.py`

### make create flow POST requests script

`python api_flows_server_calls.py`

## Development notes

RUN `prefect agent start`

# AWS

ports opened 3000 3001 8080 8081

```
ssgaw-circles-lab-01 10.30.1.101
ssh 10.30.1.101
```

## steps done

```
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
echo 'export PYENV_ROOT="~/.pyenv"' >> ~/.bash_profile
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile
git clone https://github.com/pyenv/pyenv-virtualenv.git /home/joel.lim/.pyenv/plugins/pyenv-virtualenv
echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bash_profile

```

```
sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev
```

add key in bitbucket

```
ssh-keygen
eval $(ssh-agent)
ssh-add ~/.ssh/id_rsa
```