from prefect import Flow, task
from prefect.engine import signals
import requests
import time
from job import get_job_status
import rmq_connector

server = "http://localhost:3001"
callback_server = "http://localhost:3003"


@task(name="Trigger Fetch BRM", log_stdout=True)
def trigger_brm():
    requests.get("{}/fetch-files-from-brm-to-billing-fs".format(server), params={
        'callback': "{}/set_job_status?job_identifier={}&status=complete".format(callback_server, "BRM_JOB_1")
    })


BRM_JOB_1 = "BRM_JOB_1"

@task(name="Wait Fetch BRM Response", log_stdout=True)
def wait_brm_response():
    def rmq_callback(ch, method, properties, body):
            print ("Got message")
            print (body)
            raise signals.SUCCESS(message="Finally")

    rmq_connector.start_consumer(rmq_callback)



with Flow('Automate Billing') as flow:
    trigger = trigger_brm()
    wait = wait_brm_response()
flow.register(project_name="AutoMate")
