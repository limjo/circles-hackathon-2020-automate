from api_flow import make_flow
import sys

server = "http://localhost:3001"

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "-i":
        flow_name = input("type flow name: ")
    else:
        flow_name = "A Good Flow"
    make_flow("{} from script".format(flow_name), [
        {
            "name": "Invoice for 10 ppl",
            "url": "{}".format(server),
            "method": "get",
            "params": {
                "job_size": 10
            }
        },
        {
            "name": "Payment for 10 ppl",
            "url": server,
            "method": "{}".format(server),
            "params": {
                "job_size": 10
            }
        }
    ])
