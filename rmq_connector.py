import pika, sys, os 

def create_connection():
    rmqUrl = "amqp://guest:guest@localhost:5672/billing"
    connection_params = pika.URLParameters(rmqUrl)    
    connection = pika.BlockingConnection(connection_params)
    channel = connection.channel()

    exchange_name = 'billing'
    queue_name = 'billing_result'
    routing_key = 'billing'

    channel.exchange_declare(exchange=exchange_name, exchange_type='direct')

    channel.queue_declare(queue=queue_name, durable=True, exclusive=False)
    channel.queue_bind(queue=queue_name, exchange=exchange_name, routing_key=routing_key)

    return channel

def start_consumer(consume_callback):
    print ("Starting rmq consumer")
    queue_name = 'billing_result'
    channel = create_connection()
    channel.basic_consume(queue=queue_name, on_message_callback=consume_callback, auto_ack=True)
    channel.start_consuming()
