from flask import Flask, request
from job import set_job_status
app = Flask(__name__)

@app.route('/set_job_status')
def finished():
    job_identifier = request.args.get('job_identifier')
    status = request.args.get('status')

    set_job_status(job_identifier, status)
    return 'thanks'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3003)