import json

def read_data():
    try:
        with open('data.json') as json_file:
            return json.load(json_file)
    except:
        return {}

def write_data(data):
    with open('data.json', 'w') as outfile:
        json.dump(data, outfile)

def get_job_status(identifier):
    return read_data()[identifier]

def set_job_status(identifier, status):
    data = read_data()
    data[identifier] = status
    write_data(data)