import requests
import api_flow_config

port = api_flow_config.api_flows["port"]

server = "http://localhost:{}".format(port)

res = requests.post("{}/api-flow".format(server), json={
    "flow_name": "My Good Flow from POST",
    "api_task_params": [
        {
            "name": "Invoice for 10 ppl",
            "url": "{}".format(server),
            "method": "get",
            "params": {
                "job_size": 10
            }
        },
        {
            "name": "Payment for 10 ppl",
            "url": server,
            "method": "{}".format(server),
            "params": {
                "job_size": 10
            }
        },
        {
            "name": "Some other step",
            "url": server,
            "method": "{}".format(server),
            "params": {
                "job_size": 10
            }
        }
    ]
})

print(res)
