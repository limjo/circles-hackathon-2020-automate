from prefect import Task, Flow
from prefect.core import Edge
import requests
from typing import Iterable, TypedDict
from slugify import slugify
import json
import logging

class ApiTaskParams(TypedDict):
    url: str
    method: str
    name: str
    params: dict


class ApiTask(Task):
    name: str
    url: str
    method: str
    params: dict

    def __init__(self, api_task_params: ApiTaskParams):
        super().__init__()
        self.url = api_task_params["url"]
        self.name = api_task_params["name"]
        self.method = api_task_params["method"].upper()
        self.params = api_task_params["params"]
        self.tags = []
        self.slug = slugify(self.url + self.name + self.method + json.dumps(self.params))
        self.logger = logging

    def run(self):
        print("Running task {}".format(self.name))
        if self.method == "GET":
            return requests.get(self.url, params=self.params)
        elif self.method == "POST":
            return requests.post(self.url, params=self.params)
        print("did not run {}".format(self.name))

def make_flow(flow_name, api_tasks_params: Iterable[ApiTaskParams], project_name="AutoMate"):
    tasks = [ApiTask(params)
                                      for params in api_tasks_params]
    
    edges = []
    
    for i in range(len(tasks) - 1):
        edges.append(Edge(tasks[i], tasks[i+1]))
    
    new_flow = Flow(name=flow_name, edges=edges, tasks=tasks)
    res = new_flow.register(project_name=project_name)
    print("made flow: {}".format(new_flow.name))
    return res
