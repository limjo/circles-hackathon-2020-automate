from prefect import Flow, task, Parameter
from datetime import timedelta
import requests
import time
from job import get_job_status

server = "http://localhost:3001"
callback_server = "http://localhost:3003"


@task(name="Payment Round", max_retries=3, retry_delay=timedelta(seconds=5))
def payment_round(no_of_people):
    for _ in range(no_of_people):
        time.sleep(1)
    return list(range(no_of_people))

@task(name="Payment Posting", max_retries=3, retry_delay=timedelta(seconds=5))
def payment_posting(json):
    # emails = ['hikka@circles.asia', 'vipinsharma@circles.asia', 'delbert@circles.asia', 'carrie@circles.asia']
    emails = ['joel.lim@circles.asia', 'cut@circles.asia']
    for email in emails:
        time.sleep(3)
        requests.get("{}/payment".format(server), params={
            'email': email,
            'callback': "{}/set_job_status?job_identifier={}&status=complete".format(callback_server, "BRM_JOB_1")
        })
    return emails

@task(name="Invoice Dispatch", max_retries=3, retry_delay=timedelta(seconds=5))
def invoice_dispatch(payment_res):
    # emails = ['cut@circles.asia', 'cutmeurahrudi@gmail.com', 'hikka@circles.asia', 'joel.lim@circles.asia']
    emails = ['hikka@circles.asia', 'vipinsharma@circles.asia', 'delbert@circles.asia', 'carrie@circles.asia']
    emails = ['joel.lim@circles.asia', 'cut@circles.asia']
    for email in emails:
        requests.get("{}/email".format(server), params={
            'email': email,
            'callback': "{}/set_job_status?job_identifier={}&status=complete".format(callback_server, "BRM_JOB_1")
        })

with Flow('Billing Workflow') as flow:
    no_of_people = Parameter("Number of people")
    payment_result = payment_round(no_of_people)
    posting_result = payment_posting(payment_result)
    invoice_dispatch(posting_result)

flow.register(project_name="AutoMate")