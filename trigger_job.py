# what a flow is supposed to do
# trigger external servers and give callbacks

import requests
import time
from job import get_job_status, set_job_status

server = "http://localhost:3001"

callback_server = "http://localhost:3003"

set_job_status("BRM_JOB_1", "waiting_complete")

r = requests.get("{}/fetch-files-from-brm-to-billing-fs".format(server), params={
    'callback': "{}/set_job_status?job_identifier={}&status=complete".format(callback_server, "BRM_JOB_1")
})

def wait_brm_response():
    # While loop
    while(True):
        print("Checking {} job status".format("BRM_JOB_1"))
        status = get_job_status("BRM_JOB_1")
        if status == "complete":
            print("completed {}".format("BRM_JOB_1"))
            break
        time.sleep(2)

wait_brm_response()