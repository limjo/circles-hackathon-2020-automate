from prefect import Flow, task, Parameter
from time import sleep
import requests

server = "http://localhost:3001"

@task(name="Say Hello")
def hello():
    print("hello")

@task(name="Start action to x people")
def start_action_to(no_of_people):
    print("no of people", no_of_people)
    return requests.get("{}/invoice-dispatch".format(server), params={"no_of_people": no_of_people})

with Flow('Automate Billing') as flow:
    no_of_people = Parameter("Number of people")
    h = hello()
    a = start_action_to(no_of_people)

flow.register(project_name="AutoMate")